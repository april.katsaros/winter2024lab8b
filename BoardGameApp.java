import java.util.Scanner;

public class BoardGameApp {
	public static void main(String[] args){
		//creating board object and scanner
		Board board = new Board();
		Scanner reader = new Scanner(System.in);

		System.out.println("Hello!");
		int numCastles = 7;
		int turns = 0;
		
		//runs game while there are castles on the board and the turns are less than 8
		while (numCastles > 0 && turns < 8) {
			
			System.out.println(board);
			System.out.println("Number of castles: " + numCastles);
			System.out.println("Number of turns: " + turns);
			
			//asks user to choose a square to put the token, then uses that position to call placeToken and get a result
			System.out.println("Please enter a row to place a token on (1-5): ");
			int row = Integer.parseInt(reader.nextLine()) - 1;
		
			System.out.println("Please enter a column to place a token on (1-5): ");
			int col = Integer.parseInt(reader.nextLine()) - 1;
			
			int resultOfTokenPlacement = board.placeToken(row, col);
			
			//checks result of where the token was placed and reports it to the user, adds one to turn and removes a castle if places successfully
			if (resultOfTokenPlacement == 1) {
				System.out.println("There was a wall at that position.");
				turns++;
			}
			
			else if (resultOfTokenPlacement == 0) {
				System.out.println("The castle tile was successfully placed!");
				turns++;
				numCastles--;
			}
		}
		
		//prints final board and win/loss message
		System.out.println(board);
		
		if (numCastles == 0) {
			System.out.println("Congrats, you won!");
		}
		else {
			System.out.println("You lost!");
		}
		
		
	}
}