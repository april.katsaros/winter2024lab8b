import java.util.Random;

public class Board {
	
	private Tile[][] grid;
	private final int ROWS;
	private final int COLUMNS;
	
	public Board() {
		
		ROWS = 5;
		COLUMNS = 5;
		
		this.grid = new Tile[ROWS][COLUMNS];
		
		intializeGrid();
		
		Random rng = new Random();
		
		for (int i = 0; i < grid.length; i++){
			int randIndex = rng.nextInt(grid[i].length);
			
			for (int j = 0; j < grid[i].length; j++){
				
				if (j == randIndex) {
					grid[i][j] = Tile.HIDDEN_WALL;
				}
				else {
					grid[i][j] = Tile.BLANK;
				}
			}
		}
	}
	
	// Method to initialize the grid and set all positions to blank
	private void intializeGrid() {
		
		for(int i = 0; i < this.grid.length; i++) {
			
			for(int j = 0; j < this.grid[i].length; j++) {
				
				this.grid[i][j] = Tile.BLANK;
			}
			
		}
		
	}
	
	// Prints the grid
	public String toString() {
		
		String result = "";
		
		for(Tile[] row : this.grid) {
			
			result += "\n";
			
			for(Tile col : row) {
				
				result += "[" + col.getName() + "] ";
			}
			
			result += "\n";
		}
		
		return result;
	}
	
	// If the placement of the token is invalid, it returns false, if not, it places the token.
	public int placeToken(int row, int col) {
		
		if (row < 0 || row > ROWS - 1 || col < 0 || col > COLUMNS - 1) {
			return -2;
		}
		
		else if (this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL) {
			return -1;
		}
		
		else if (this.grid[row][col] == Tile.HIDDEN_WALL) {
			this.grid[row][col] = Tile.WALL;
			return 1;
			
		}
		
		else if (this.grid[row][col] == Tile.BLANK) {
			this.grid[row][col] = Tile.CASTLE;
			return 0;
			
		}
		
		return -2;
	}
}